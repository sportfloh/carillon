// define the output for each tone

enum class Notes {
  C2 = 0,
  D2 = 1,
  E2 = 2,
  F2 = 3,
  G2 = 4,
  A2 = 5,
  B2 = 6,
  C3 = 7,
  D3 = 8,
  E3 = 9,
};

// definition of note values
enum class NoteValues {
  WHOLE_NOTE = 0,
  HALF_NOTE = 1,
  QUARTER_NOTE = 2,
  EIGHTH_NOTE = 3,
  SIXTEENTH_NOTE = 4,
};

// other defines
constexpr uint32_t MIN{60000};

// global variables
uint16_t notevaluedelay{};

/**
 * setup stuff 
 */
void setup() {
    // set pins to output mode
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode((uint8_t)Notes::C2, OUTPUT);
    pinMode((uint8_t)Notes::D2, OUTPUT);
    pinMode((uint8_t)Notes::E2, OUTPUT);
    pinMode((uint8_t)Notes::F2, OUTPUT);
    pinMode((uint8_t)Notes::G2, OUTPUT);
    pinMode((uint8_t)Notes::A2, OUTPUT);
    pinMode((uint8_t)Notes::B2, OUTPUT);
    pinMode((uint8_t)Notes::C3, OUTPUT);
    pinMode((uint8_t)Notes::D3, OUTPUT);
    pinMode((uint8_t)Notes::E3, OUTPUT);

    digitalWrite((uint8_t)Notes::C2, HIGH);
    digitalWrite((uint8_t)Notes::D2, HIGH);
    digitalWrite((uint8_t)Notes::E2, HIGH);
    digitalWrite((uint8_t)Notes::F2, HIGH);
    digitalWrite((uint8_t)Notes::G2, HIGH);
    digitalWrite((uint8_t)Notes::A2, HIGH);
    digitalWrite((uint8_t)Notes::B2, HIGH);
    digitalWrite((uint8_t)Notes::C3, HIGH);
    digitalWrite((uint8_t)Notes::D3, HIGH);
    digitalWrite((uint8_t)Notes::E3, HIGH);

    // set default values
    notevaluedelay = 100;
}

/**
 * main loop
 */
void loop() {
    delay(1000);

    tonleiter(100);
    delay(5 * MIN);

    allemeineentchen(100);
    delay(5 * MIN);

    dievogelhochzeit(100);
    delay(5 * MIN);

    whenthesaintsgomarchingin(100);
    delay(5 * MIN);    
}

/**
 * pause 
 */
void pause(const NoteValues& notevalue) {
    switch (notevalue) {
        case NoteValues::WHOLE_NOTE:
            delay(notevaluedelay * 4);
            break;
        case NoteValues::HALF_NOTE:
            delay(notevaluedelay * 2);
            break;
        case NoteValues::QUARTER_NOTE:
            delay(notevaluedelay);
            break;
        case NoteValues::EIGHTH_NOTE:
            delay(notevaluedelay / 2);
            break;
        case NoteValues::SIXTEENTH_NOTE:
            delay(notevaluedelay / 4);
            break;
        default:
            break;
    }
}

/**
 * play tone by pitch and notevalue
 */
void play(const Notes& tone, const NoteValues& notevalue) {
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite((uint8_t)tone, LOW);
    delay(300);
    digitalWrite((uint8_t)tone, HIGH);

    switch (notevalue) {
        case NoteValues::WHOLE_NOTE:
            delay((int)notevaluedelay * 4);
            break;
        case NoteValues::HALF_NOTE:
            delay((int)notevaluedelay * 2);
            break;
        case NoteValues::QUARTER_NOTE:
            delay((int)notevaluedelay);
            break;
        case NoteValues::EIGHTH_NOTE:
            delay((int)notevaluedelay / 2);
            break;
        case NoteValues::SIXTEENTH_NOTE:
            delay((int)notevaluedelay / 4);
            break;
        default:
            break;
    }
    digitalWrite(LED_BUILTIN, LOW);
}

/**
 * song: tonleiter
 */
void tonleiter(const int& speed) {
    notevaluedelay = MIN / speed;

    play(Notes::C2, NoteValues::QUARTER_NOTE);
    play(Notes::D2, NoteValues::QUARTER_NOTE);
    play(Notes::E2, NoteValues::QUARTER_NOTE);
    play(Notes::F2, NoteValues::QUARTER_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);
    play(Notes::A2, NoteValues::QUARTER_NOTE);
    play(Notes::B2, NoteValues::QUARTER_NOTE);
    play(Notes::C3, NoteValues::QUARTER_NOTE);
    play(Notes::D3, NoteValues::QUARTER_NOTE);
    play(Notes::E3, NoteValues::QUARTER_NOTE);
    pause(NoteValues::QUARTER_NOTE);

    play(Notes::E3, NoteValues::QUARTER_NOTE);
    play(Notes::D3, NoteValues::QUARTER_NOTE);
    play(Notes::C3, NoteValues::QUARTER_NOTE);
    play(Notes::B2, NoteValues::QUARTER_NOTE);
    play(Notes::A2, NoteValues::QUARTER_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);
    play(Notes::F2, NoteValues::QUARTER_NOTE);
    play(Notes::E2, NoteValues::QUARTER_NOTE);
    play(Notes::D2, NoteValues::QUARTER_NOTE);
    play(Notes::C2, NoteValues::QUARTER_NOTE);
}

/**
 * song: Alle meine Entchen
 */
void allemeineentchen(const int& speed) {
    // calculate notevaluedelay
    notevaluedelay = MIN / speed;

    // notes
    play(Notes::C2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);

    play(Notes::A2, NoteValues::EIGHTH_NOTE);
    play(Notes::A2, NoteValues::EIGHTH_NOTE);
    play(Notes::A2, NoteValues::EIGHTH_NOTE);
    play(Notes::A2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);
    pause(NoteValues::QUARTER_NOTE);

    play(Notes::A2, NoteValues::EIGHTH_NOTE);
    play(Notes::A2, NoteValues::EIGHTH_NOTE);
    play(Notes::A2, NoteValues::EIGHTH_NOTE);
    play(Notes::A2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);
    pause(NoteValues::QUARTER_NOTE);

    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::QUARTER_NOTE);
    play(Notes::E2, NoteValues::QUARTER_NOTE);

    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::C2, NoteValues::WHOLE_NOTE);
}

/**
 * song: Die Vogle Hochzeit
 */
void dievogelhochzeit(const int& speed) {
    notevaluedelay = MIN / speed;

    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::EIGHTH_NOTE);

    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);

    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::C2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::QUARTER_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);

    play(Notes::G2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::C2, NoteValues::EIGHTH_NOTE);
    play(Notes::C2, NoteValues::EIGHTH_NOTE);
    play(Notes::C2, NoteValues::QUARTER_NOTE);

    play(Notes::G2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::QUARTER_NOTE);

    play(Notes::G2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::C3, NoteValues::EIGHTH_NOTE);
    play(Notes::D3, NoteValues::EIGHTH_NOTE);
    play(Notes::B2, NoteValues::EIGHTH_NOTE);
    play(Notes::C3, NoteValues::WHOLE_NOTE);
    play(Notes::C2, NoteValues::WHOLE_NOTE);
 }

/**
 * song: When the Saints go marching in
 */
void whenthesaintsgomarchingin(const int& speed) {
    notevaluedelay = MIN / speed;

    play(Notes::C2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);

    play(Notes::C2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);

    play(Notes::C2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);
    play(Notes::E2, NoteValues::QUARTER_NOTE);
    play(Notes::C2, NoteValues::QUARTER_NOTE);
    play(Notes::E2, NoteValues::QUARTER_NOTE);
    play(Notes::D2, NoteValues::QUARTER_NOTE);

    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::D2, NoteValues::EIGHTH_NOTE);
    play(Notes::C2, NoteValues::QUARTER_NOTE);

    play(Notes::C2, NoteValues::QUARTER_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);
    play(Notes::G2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::HALF_NOTE);

    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::E2, NoteValues::EIGHTH_NOTE);
    play(Notes::F2, NoteValues::EIGHTH_NOTE);
    play(Notes::G2, NoteValues::QUARTER_NOTE);
    play(Notes::E2, NoteValues::QUARTER_NOTE);
    play(Notes::C2, NoteValues::QUARTER_NOTE);
    play(Notes::D2, NoteValues::QUARTER_NOTE);
    play(Notes::C2, NoteValues::WHOLE_NOTE);
}
